package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"log"
	"math"
	"math/big"
)

const TargetsBits = 20

type ProofOfWork struct {
	Block  *Block
	Target *big.Int
}

func NewProof(block *Block) *ProofOfWork {
	target := big.NewInt(1)
	target.Lsh(target, uint(256-TargetsBits))
	proof := &ProofOfWork{block, target}
	return proof
}

func (proof *ProofOfWork) SetData(nonce int) []byte {
	data := bytes.Join([][]byte{proof.Block.LstBlckHash, proof.Block.Data,
		IntToHex(int64(TargetsBits)), IntToHex(int64(nonce))}, []byte{})

	return data
}

func (proof *ProofOfWork) Run() (int, []byte) {
	var intOfHash big.Int
	var hash [32]byte
	nonce := 0
	maxOnce := math.MaxInt64
	fmt.Println("------------------------------------------------------------------------------------")
	fmt.Printf("Mining a block with \"%s\"\n", proof.Block.Data)
	for nonce < maxOnce {
		data := proof.SetData(nonce)
		hash = sha256.Sum256(data)
		fmt.Printf("\r%x", hash)
		intOfHash.SetBytes(hash[:])

		if intOfHash.Cmp(proof.Target) == -1 {
			break
		} else {
			nonce++
		}
	}
	fmt.Println("\n\n")
	return nonce, hash[:]
}

func IntToHex(num int64) []byte {
	buffer := new(bytes.Buffer)
	err := binary.Write(buffer, binary.BigEndian, num)
	if err != nil {
		log.Panic(err)
	}
	return buffer.Bytes()
}

func (proof *ProofOfWork) Validate() bool {
	var intOfHash big.Int
	data := proof.SetData(proof.Block.Nonce)
	hash := sha256.Sum256(data)
	intOfHash.SetBytes(hash[:])
	return intOfHash.Cmp(proof.Target) == -1
}
