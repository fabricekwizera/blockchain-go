package blockchain

import (
	"bytes"
	"encoding/gob"
	"log"
)

// Defining the essential parts of a "Block" and those are the hash of the current block
// the data in the current block and the hash of the last block
type Block struct {
	CurrBlckHash []byte
	Data         []byte
	LstBlckHash  []byte
	Nonce        int
}

func CreateBlock(data string, lstBlckHash []byte) *Block {
	block := &Block{[]byte{}, []byte(data), lstBlckHash, 0}
	proof := NewProof(block)
	nonce, hash := proof.Run()
	block.Nonce = nonce
	block.CurrBlckHash = hash[:]
	return block
}

// Generating the first block of the chain
func GenesisBlock() *Block {
	return CreateBlock("Genesis block", []byte{})
}

// Converting blocks (type struct) into []byte to ease storage in the DB
func (block *Block) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)
	err := encoder.Encode(block)
	if err != nil {
		log.Panic(err)
	}
	return result.Bytes()
}

// Converting []byte to block
func Deserialize(data []byte) *Block {
	var block Block
	decoder := gob.NewDecoder(bytes.NewReader(data))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}
	return &block
}

func ErrorHandler(err error) {
	if err != nil {
		log.Panic(err)
	}
}
