package blockchain

import (
	"fmt"

	"github.com/dgraph-io/badger"
)

const databasePath = "./database"

type Blockchain struct {
	LstBlckHash []byte
	Database    *badger.DB
}

type BlockchainIterator struct {
	CurrentBlckHash []byte
	Database        *badger.DB
}

func (chain *Blockchain) AddBlock(data string) {
	var lstBlckHash []byte
	err := chain.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte("lh"))
		ErrorHandler(err)
		lstBlckHash, err = item.ValueCopy(nil)
		return err
	})
	ErrorHandler(err)
	newBlock := CreateBlock(data, lstBlckHash)
	err = chain.Database.Update(func(txn *badger.Txn) error {
		err := txn.Set(newBlock.CurrBlckHash, newBlock.Serialize())
		ErrorHandler(err)
		err = txn.Set([]byte("lh"), newBlock.CurrBlckHash)
		chain.LstBlckHash = newBlock.CurrBlckHash
		return err
	})
	ErrorHandler(err)
}

// func CreateBlockchain() *Blockchain {
// 	return &Blockchain{[]*Block{GenesisBlock()}}
// }

func CreateBlockchain() *Blockchain {
	var lstBlckHash []byte
	options := badger.DefaultOptions(databasePath)
	options.Dir = databasePath
	options.ValueDir = databasePath
	database, err := badger.Open(options)
	ErrorHandler(err)
	err = database.Update(func(txn *badger.Txn) error {
		if _, err := txn.Get([]byte("lh")); err == badger.ErrKeyNotFound {
			fmt.Println()
			fmt.Println("-------------------  No blockchain was found ---------------------")
			genesisBlock := GenesisBlock()
			fmt.Println("----------------- Genesis block generated ----------------------")
			err = txn.Set(genesisBlock.CurrBlckHash, genesisBlock.Serialize())
			ErrorHandler(err)
			err = txn.Set([]byte("lh"), genesisBlock.CurrBlckHash)
			lstBlckHash = genesisBlock.CurrBlckHash
			return err
		} else {
			item, err := txn.Get([]byte("lh"))
			lstBlckHash, err = item.ValueCopy(nil)
			return err
		}

	})
	ErrorHandler(err)
	blockchain := Blockchain{lstBlckHash, database}
	return &blockchain
}

// Converting Blockchain struct to BlockchainIterator struct
func (chain *Blockchain) Iterator() *BlockchainIterator {
	iter := &BlockchainIterator{chain.LstBlckHash, chain.Database}
	return iter
}

// Iterating from the latest block to the Blockchain to the first block (Genesis)
// using a read-only transaction on the Badger DB
func (iter *BlockchainIterator) Next() *Block {
	var block *Block
	err := iter.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get(iter.CurrentBlckHash)
		encodedBlock, err := item.ValueCopy(nil)
		block = Deserialize(encodedBlock)
		return err
	})
	ErrorHandler(err)
	iter.CurrentBlckHash = block.LstBlckHash
	return block
}
