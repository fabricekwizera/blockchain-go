package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strconv"

	"bitbucket.org/fabricekwizera/blockchain-go/blockchain"
)

type CommandLine struct {
	blockchain *blockchain.Blockchain
}

func (commandLine *CommandLine) menu() {
	fmt.Println("Usage: ")
	fmt.Println("Add block to the chain ")
	fmt.Println("Prints the blocks in the chain ")
}

func (commandLine *CommandLine) verifyArguments() {
	if len(os.Args) < 2 {
		commandLine.menu()
		runtime.Goexit()
	}
}

func (commandLine *CommandLine) addBlock(data string) {
	commandLine.blockchain.AddBlock(data)
	fmt.Printf("Added block with \"%s\"", data)
}

func (commandLine *CommandLine) printChain() {
	iter := commandLine.blockchain.Iterator()
	for {
		block := iter.Next()
		fmt.Println("---------------------------------------------------------------------------------------------------")
		fmt.Printf("Hash of previous block: %x\n", block.LstBlckHash)
		fmt.Printf("Data in current block: %s\n", block.Data)
		fmt.Printf("Hash of current block: %x\n", block.CurrBlckHash)
		proofOfWork := blockchain.NewProof(block)
		fmt.Printf("Proof of Work : %s\n", strconv.FormatBool(proofOfWork.Validate()))
		fmt.Println("---------------------------------------------------------------------------------------------------")
		if len(block.LstBlckHash) == 0 {
			break
		}
	}

}

func (commandLine *CommandLine) run() {
	commandLine.verifyArguments()
	addBlockCommand := flag.NewFlagSet("add", flag.ExitOnError)
	printChainCommand := flag.NewFlagSet("print", flag.ExitOnError)
	addBlockData := addBlockCommand.String("block", "", "Block data")
	switch os.Args[1] {
	case "add":
		err := addBlockCommand.Parse(os.Args[2:])
		blockchain.ErrorHandler(err)

	case "print":
		err := printChainCommand.Parse(os.Args[2:])
		blockchain.ErrorHandler(err)

	default:
		commandLine.menu()
		runtime.Goexit()
	}
	if addBlockCommand.Parsed() {
		if *addBlockData == "" {
			addBlockCommand.Usage()
			runtime.Goexit()
		}
		commandLine.addBlock(*addBlockData)
	}
	if printChainCommand.Parsed() {
		commandLine.printChain()
	}

}

func main() {
	defer os.Exit(0)
	chain := blockchain.CreateBlockchain()
	defer chain.Database.Close()

	commandLine := CommandLine{chain}
	commandLine.run()
}
