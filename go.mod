module bitbucket.org/fabricekwizera/blockchain-go

go 1.13

require (
	github.com/dgraph-io/badger v1.6.0
	github.com/sirupsen/logrus v1.4.2
	rsc.io/quote v1.5.2 // indirect
)
