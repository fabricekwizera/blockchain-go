# BLOCKCHAIN - GO #

## What is the project for? ##

This project is simplified implementation of Blockchain technology, one of the most revolutionary of the 21st century. This technology is known for giving birth to cryptocurrencies like [Bitcoin (BTC)](https://bitcoin.org/en/), [Ethereum (ETH)](https://ethereum.org/), [Stellar (XLM)](https://www.stellar.org/) and many more.


## What is really done in the project? ##

* A BLOCK: Defined as type struct with its parts such two hashes, the one of the current block and the other of the previous block as well as data stored in it. Its structure is improved throughout the implementation of the project.
* 